package com.example.plugins

import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.response.*

fun Application.configureRouting() {


    routing {
        get("/") {
            call.respondText("Hello, I am pong service!")
        }
        get("/pong") {
            call.respondText("Pong")
        }
    }
}

//@app.route('/')
//def ping_service():
//    return 'Hello, I am ping service!'
//
//@app.route('/ping')
//def do_ping():
//    ping = 'Ping ...'
//
//    response = ''
//    try:
//        response = requests.get('http://pong-service-container:5001/pong')
//    except requests.exceptions.RequestException as e:
//        print('\n Cannot reach the pong service.')
//        return 'Ping ...\n'
//
//    return 'Ping ... ' + response.text + ' \n'
