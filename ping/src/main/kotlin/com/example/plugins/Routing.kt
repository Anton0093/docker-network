package com.example.plugins

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.request
import io.ktor.client.statement.bodyAsText
import io.ktor.server.routing.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*

fun Application.configureRouting() {

    routing {

        get("/") {
            call.respondText("Hello, I am ping service!")
        }

        get("/ping") {
            val client = HttpClient(CIO) {
                expectSuccess = true
            }
            try {
                val response = client.request("http://localhost:8080/") {
                    method = HttpMethod.Get
                }
                call.respondText("Ping ...  ${response.bodyAsText()}  \n")
            } catch (e: Exception) {
                println("Exception: ${e.message} ")
            } finally {
                client.close()
            }
        }
    }
}
